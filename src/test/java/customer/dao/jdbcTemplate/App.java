
package customer.dao.jdbcTemplate;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.customer.dao.impl.JdbcTemplateCustomerDAO;
import com.customer.model.Customer;

public class App {
	
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("Spring-Customer.xml");
		
		JdbcTemplateCustomerDAO customerDAO = (JdbcTemplateCustomerDAO) context.getBean("customerDAO");
		Customer customer1 = new Customer(1, "mkyong1", 21);
		Customer customer3 = new Customer(2, "mkyong2", 22);
		Customer customer2 = new Customer(3, "mkyong3", 23);
		
		List<Customer> customers = new ArrayList<Customer>();
		customers.add(customer1);
		customers.add(customer2);
		customers.add(customer3);
		
		customerDAO.insertBatch(customers);
		
		String sql = "UPDATE CUSTOMER SET NAME ='BATCHUPDATE'";
		customerDAO.insertBatchSQL(sql);
		
	}
}
